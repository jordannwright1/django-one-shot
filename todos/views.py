from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemCreateForm


def get_list(request):
    name = TodoList.objects.all()
    context = {
        "name": name
    }
    return render(request, "todos/list.html", context)


def get_item(request, id):
    item = get_object_or_404(TodoList, id=id)
    context = {
        "item": item,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect('todo_list_detail', todolist.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    update = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=update)
        if form.is_valid():
            updated_list = form.save()
            return redirect('todo_list_detail', id=updated_list.id)
    else:
        form = TodoListForm(instance=update)
    context = {
        'form': form
    }
    return render(request, 'todos/update.html', context)


def todo_list_delete(request, id):
    instance = TodoList.objects.get(id=id)
    if request.method == 'POST':
        instance.delete()
        return redirect('todo_list_list')
    return render(request, 'todos/delete.html')


def todo_item_create(request):
    if request.method == 'POST':
        form = TodoItemCreateForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = TodoItemCreateForm()
    context = {
        'form': form
    }
    return render(request, 'items/create.html', context)


def todo_item_update(request, id):
    current = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        form = TodoItemCreateForm(request.POST, instance=current)
        if form.is_valid():
            updated_item = form.save()
            return redirect('todo_list_detail', id=updated_item.id)
    else:
        form = TodoItemCreateForm(instance=current)
    context = {
        'form': form,
    }
    return render(request, 'items/edit.html', context)
