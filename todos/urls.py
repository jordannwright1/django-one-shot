from todos.views import get_list, get_item, create_list, todo_list_update, todo_list_delete, todo_item_create, todo_item_update
from django.urls import path

urlpatterns = [
    path("", get_list, name="todo_list_list"),
    path("items/create/", todo_item_create, name='todo_item_create'),
    path("<int:id>/", get_item, name="todo_list_detail"),
    path("create/", create_list, name="todo_list_create"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
]
